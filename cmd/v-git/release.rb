#!/usr/local/bin/ruby
# coding: utf-8
def cmdline(command)
  puts "command execution : #{command}"
  if !system(command)
    raise "command execution: #{command} fail !"
  end
end
baseDir = File.expand_path(ARGV[0])
puts "base directory:#{baseDir}"
if !File.directory?(baseDir)
  raise "base directory : #{baseDir} does not exist"
end
puts "************************* release the file to current release directory *******************************"
application = ARGV[1]
applicationDir = "#{baseDir}/#{application}"
sourcesDir = "#{baseDir}/sources"
currentDir = "#{baseDir}/#{application}/current"
releasedDir = "#{baseDir}/#{application}/released"
rollbackedDir = "#{baseDir}/#{application}/rollbacked"
puts "application name:#{application}"
puts "application directory:#{applicationDir}"
targetDir = "#{sourcesDir}/#{application}/target"
#  create history dir structure
if !File.exist?(applicationDir)
  Dir.mkdir(applicationDir)
  Dir.mkdir(currentDir)
  Dir.mkdir(releasedDir)
  Dir.mkdir(rollbackedDir)
end
# find war file
war = ""
Dir["#{targetDir}/#{application}-*.war"].each do |f|
  war = f
end
puts "current packaged file : #{war}"
puts "************************* history check *******************************"
if File.exist?(war)
  ignoreRelease = false;
  if Dir["#{currentDir}/*"].length > 0
    # 检查war文件是否有变化
    curWar = ""
    Dir["#{currentDir}/#{application}-*.war"].each do |f|
      curWar = f
    end
    if File.exist?(curWar) && File.mtime(war)==File.mtime(curWar) && File.size(war)==File.size(curWar)
      puts "same with current file, ignore it and will not release."
      ignoreRelease = true;
    end
    if !ignoreRelease
      # record release history
      newReleasedDir = "#{releasedDir}/#{Time.now.to_i}"
      Dir.mkdir(newReleasedDir)
      puts "copy from #{currentDir} to #{newReleasedDir}"
      cmdline("cp -r #{currentDir}/* #{newReleasedDir}")
      puts "clean current directory #{currentDir}"
      cmdline("rm -r #{currentDir}")
      cmdline("mkdir #{currentDir}")
    end
  end
  if !ignoreRelease
  # copy war to current file
  puts "copy packaged file to #{currentDir}"
  cmdline("cp -p #{war} #{currentDir}/#{File.basename(war)}")
  end
else
  raise "packaged file #{application}-x.x.x.war not in #{targetDir}"
end
puts "************************* done *******************************"

#!/usr/bin/ruby
# coding: utf-8
def cmdline(command)
  puts "命令行执行 : #{command}"
  if !system(command)
    raise "命令行执行: #{command} 失败 !"
  end
end
puts "************************* 回滚发布文件 *******************************"
baseDir = File.expand_path(ARGV[0])
puts "根目录:#{baseDir}"
if !File.directory?(baseDir)
  raise "根目录 : #{baseDir} 不存在"
end
application = ARGV[1]
applicationDir = "#{baseDir}/#{application}"
currentDir = "#{baseDir}/#{application}/current"
releasedDir = "#{baseDir}/#{application}/released"
rollbackedDir = "#{baseDir}/#{application}/rollbacked"
puts "应用名称:#{application}"
puts "应用发布目录:#{applicationDir}"
if !File.exist?(releasedDir)
  raise "released dir :#{releasedDir} not exist"
end
#  last released dir
lastReleasedDir = ""
Dir["#{releasedDir}/*"].each do |f|
  dirName = File.basename(f)
  if lastReleasedDir == ""
    lastReleasedDir = dirName
  else 
    if dirName > lastReleasedDir
      lastReleasedDir = dirName
    end
  end
end
if lastReleasedDir == ""
  raise "没有已发布的文件"
end
lastReleasedDir = "#{releasedDir}/#{lastReleasedDir}"
puts "上次发布目录 #{lastReleasedDir}"
puts "************************* 回滚文件 *******************************"
if Dir["#{currentDir}/*"].length > 0
  if !File.exist?(rollbackedDir)
    raise "回滚目录 :#{rollbackedDir} 不存在"
  end
  #  record rollback history
  newRollbackedDir = "#{rollbackedDir}/#{Time.now.to_i}"
  Dir.mkdir(newRollbackedDir)
  puts "复制当前发布目录 #{currentDir} 到回滚目录 #{newRollbackedDir}"
  cmdline("cp -r #{currentDir}/* #{newRollbackedDir}")
  puts "清空当前发布目录 #{currentDir}"
  cmdline("rm -r #{currentDir}")
  cmdline("mkdir #{currentDir}")
end
# copy last released to current
puts "复制上次发布目录到当前目录 #{currentDir}"
cmdline("cp -r #{lastReleasedDir}/* #{currentDir}")
puts "删除上次发布目录 #{lastReleasedDir}"
cmdline("rm -r #{lastReleasedDir}")
puts "************************* 回滚发布文件完成 *******************************"

#!/usr/local/bin/ruby
# coding: utf-8
def cmdline(command)
  puts "command execution : #{command}"
  if !system(command)
    raise "command execution: #{command} fail !"
  end
end
puts "************************* 恢复回滚文件 *******************************"
baseDir = File.expand_path(ARGV[0])
puts "Base Directory:#{baseDir}"
if !File.directory?(baseDir)
  raise "Base Directory : #{baseDir} does not exist"
end
application = ARGV[1]
applicationDir = "#{baseDir}/#{application}"
currentDir = "#{baseDir}/#{application}/current"
releasedDir = "#{baseDir}/#{application}/released"
rollbackedDir = "#{baseDir}/#{application}/rollbacked"
puts "application name:#{application}"
puts "application directory:#{applicationDir}"
if !File.exist?(rollbackedDir)
  raise "rollback directory :#{rollbackedDir} does not exist"
end
#  last rollbacked dir
lastRollbackedDir = ""
Dir["#{rollbackedDir}/*"].each do |f|
  dirName = File.basename(f)
  if lastRollbackedDir == ""
    lastRollbackedDir = dirName
  else 
    if dirName > lastRollbackedDir
      lastRollbackedDir = dirName
    end
  end
end
if lastRollbackedDir == ""
  raise "no such file to rollback"
end
lastRollbackedDir = "#{rollbackedDir}/#{lastRollbackedDir}"
puts "last rollbacked directory #{lastRollbackedDir}"
puts "************************* recover files *******************************"
if Dir["#{currentDir}/*"].length > 0
  if !File.exist?(releasedDir)
    raise "released directory :#{releasedDir} does not exist!"
  end
  #  record release history
  newReleasedDir = "#{releasedDir}/#{Time.now.to_i}"
  Dir.mkdir(newReleasedDir)
  puts "copy current directory #{currentDir} to release directory #{newReleasedDir}"
  cmdline("cp -r #{currentDir}/* #{newReleasedDir}")
  puts "clean current directory #{currentDir}"
  cmdline("rm -r #{currentDir}")
  cmdline("mkdir #{currentDir}")
end
# copy last rollbacked to current
puts "copy last rollbacked directory to current directory #{currentDir}"
cmdline("cp -r #{lastRollbackedDir}/* #{currentDir}")
puts "delete last rollbacked directory #{lastRollbackedDir}"
cmdline("rm -r #{lastRollbackedDir}")
puts "************************* done *******************************"

#!/usr/local/bin/ruby
# coding: utf-8
def cmdline(command)
  puts "#{command}"
  if !system(command)
    raise "ERROR: #{command} failed!"
  end
end
if ARGV.length < 2
  raise "ERROR: autodeploy project application"
end
index = 1
while (index < ARGV.length)
  cmdline("release #{ARGV[0]} #{ARGV[index]}")
  cmdline("deploy #{ARGV[0]} #{ARGV[index]}")
  index = index + 1
end

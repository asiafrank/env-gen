#!/usr/local/bin/ruby
# coding: utf-8
require "yaml"
def cmdline(command)
  puts "execute: #{command}"
  if !system(command)
    raise "ERROR: #{command} failed!"
  end
end
def deploy(baseDir,war,server,application)
    ss = server.chomp.split(/;/)
    if ss.length == 2
      server = ss[0]
      tomcat = ss[1]
    else
      server = ss[0]
    end
    puts "send war file"
    cmdline("scp #{war} #{server}:./#{application}.war")
    puts "send shell script file"
    deploySh = "#{baseDir}/script/#{application}.sh"
    if !File.exist?(deploySh)
      raise "ERROR: #{deploySh} does not exist!"
    end
    cmdline("scp #{deploySh} #{server}:.")
    if defined?tomcat
    puts "replace tomcatX variable"
    cmdline("ssh #{server} 'perl -p -i -e \"s/tomcatX/#{tomcat}/g\" #{application}.sh'")
    end
    puts "execute: #{server} shell script"
    cmdline("ssh #{server} 'sh #{File.basename(deploySh)}'")
end
baseDir = File.expand_path(ARGV[0])
puts "base dir: #{baseDir}"
if !File.directory?(baseDir)
  raise "ERROR: #{baseDir} does not exist"
end
application = ARGV[1]
applicationDir = "#{baseDir}/#{application}"
currentDir = "#{applicationDir}/current"
puts "application: #{application}"
puts "application dir: #{applicationDir}"
configFile = "#{baseDir}/publish.yaml"
puts "config file: #{configFile}"
if !File.exist?(configFile)
  raise "ERROR: #{configFile} does not exist"
end
config = YAML.load_file(configFile)
# copy war to server
puts "************************** deploy war ******************************"
# find war file
war = ""
Dir["#{currentDir}/#{application}-*.war"].each do |f|
  war = f
end
if war == ""
  raise "ERROR: #{war} does not exist"
end
runServer = config["runServer"];
if runServer == nil
  raise "ERROR: runServer is not config"
end
if runServer.class != Hash
  raise "ERROR: runServer config is not correct, runServer must be Hash, type: #{runServer.class}"
end
servers = config["runServer"][application];
if servers == nil
  raise "ERROR: #{application} server is not config"
end
if servers.class != Array
  raise "servers config is not correct, servers must be Array, type: #{servers.class}"
end
puts "deploy #{servers.length} server(s)"
i = 1
servers.each do |server|
  puts "  #{i}: #{server}"
  i = i + 1
end
puts "Please select server"
puts "  0: all, 1-#{servers.length}: deploy one server, q: exit"
puts "Please select:"
input = STDIN.gets.chomp()
if input == "0"
  j = 1
  servers.each do |server|
    if server.chomp.length > 0
      if j > 1
        puts "wait previous server ..."
        sleep(15)
      end
      puts "*********************** deploy the #{j} server of #{servers.length}: #{server} ***********************"
      deploy(baseDir,war,server,application)
    else
      puts "Server does not exist, you select: #{input}"
    end
    j = j + 1
  end
elsif input == "q"
  puts "cancel deployment"
elsif input.to_i > 0 && input.to_i <= servers.length
  server = servers[input.to_i - 1]
  if server.chomp.length > 0
    puts "*********************** deploy: #{server} ***********************"
    deploy(baseDir,war,server,application)
  else
    puts "server does not exist, you select: #{input}"
  end
else
  raise "ERROR: your choose is not correct: #{input}"
end

puts "**************************** completed! *****************************"

#!/usr/local/bin/ruby
# coding: utf-8
require "yaml"
def cmdline(command)
  puts "command execution : #{command}"
  if !system(command)
    raise "command execution: #{command} fail!"
  end
end
baseDir = File.expand_path(ARGV[0])
puts "base directory:#{baseDir}"
if !File.directory?(baseDir)
  raise "base directory : #{baseDir} does not exist!"
end
configFile = "#{baseDir}/publish.yaml"
puts "configuration File : #{configFile}"
if !File.exist?(configFile)
  raise "configuration File : #{configFile} does not exist!"
end
config = YAML.load_file(configFile)
codeRepoUrl = config["sourceSvnUrl"]
sourcesDir = "#{baseDir}/sources"
puts "code repository:#{codeRepoUrl}"
puts "source directory:#{sourcesDir}"
puts "************************* git pull origin master *******************************"
puts "update filter file"
Dir.chdir("/home/java/.filter")
cmdline("git pull origin master")
Dir.chdir(baseDir)
if File.exist?(sourcesDir)
  Dir.chdir(sourcesDir)
  puts "update #{sourcesDir} code"
  cmdline("git pull origin master")
  Dir.chdir(baseDir)
else
  puts "git clone from #{codeRepoUrl} to #{sourcesDir}"
  cmdline("git clone #{codeRepoUrl} #{sourcesDir}")
end
puts "************************* package project *******************************"
  Dir.chdir(sourcesDir)
  puts "package binary at #{sourcesDir}"
  cmdline("mvn -Dmaven.test.skip=true -U clean install")
  Dir.chdir(baseDir)
puts "************************* package project *******************************"

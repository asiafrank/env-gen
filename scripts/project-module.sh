#!/bin/sh

if [ -f /etc/profile ]; then
  . /etc/profile
fi
if [ -f ~/.bash_profile ]; then
  . ~/.bash_profile
fi

ROOT_PATH=/data/project-module
rm -rf $ROOT_PATH/*
cp project-module.war $ROOT_PATH
cd $ROOT_PATH
jar xf project-module.war
rm -rf project-module.war

cd ~

sh restart-tomcat.sh /data/tomcat

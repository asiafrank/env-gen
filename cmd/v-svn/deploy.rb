#!/usr/bin/ruby
# coding: utf-8
require "yaml"
def cmdline(command)
  puts "Execute: #{command}"
  if !system(command)
    raise "Error: #{command} failed!"
  end
end
def deploy(baseDir,war,server,application)
    ss = server.chomp.split(/;/)
    if ss.length == 2
      server = ss[0]
      tomcat = ss[1]
    else
      server = ss[0]
    end
    puts "Send war file"
    cmdline("cp #{war} ./#{application}.war")
    puts "Send shell script file"
    deploySh = "#{baseDir}/script/#{application}.sh"
    if !File.exist?(deploySh)
      raise "Error: #{deploySh} does not exist!"
    end
    cmdline("cp #{deploySh} .")
    if defined?tomcat
    puts "Replace tomcatX variable"
    cmdline("perl -p -i -e \"s/tomcatX/#{tomcat}/g\" #{application}.sh")
    end
    puts "Execute: #{server} shell script"
    cmdline("sh #{File.basename(deploySh)}")
end
baseDir = File.expand_path(ARGV[0])
puts "Base dir: #{baseDir}"
if !File.directory?(baseDir)
  raise "Error: #{baseDir} does not exist"
end
application = ARGV[1]
applicationDir = "#{baseDir}/#{application}"
currentDir = "#{applicationDir}/current"
puts "Application: #{application}"
puts "Application dir: #{applicationDir}"
configFile = "#{baseDir}/publish.yaml"
puts "Config file: #{configFile}"
if !File.exist?(configFile)
  raise "Error: #{configFile} does not exist"
end
config = YAML.load_file(configFile)
# copy war to server
puts "************************** deploy war ******************************"
# find war file
war = ""
Dir["#{currentDir}/#{application}-*.war"].each do |f|
  war = f
end
if war == ""
  raise "Error: #{war} does not exist"
end
runServer = config["runServer"];
if runServer == nil
  raise "Error: runServer is not config"
end
if runServer.class != Hash
  raise "Error: runServer config is not correct, runServer must be Hash, type: #{runServer.class}"
end
servers = config["runServer"][application];
if servers == nil
  raise "Error: #{application} server is not config"
end
if servers.class != Array
  raise "servers config is not correct, servers must be Array, type: #{servers.class}"
end
puts "deploy #{servers.length} server(s)"
i = 1
servers.each do |server|
  puts "  #{i}: #{server}"
  i = i + 1
end
puts "Please select server"
puts "  0: all, 1-#{servers.length}: deploy one server, q: exit"
puts "Please select:"
input = STDIN.gets.chomp()
if input == "0"
  j = 1
  servers.each do |server|
    if server.chomp.length > 0
      if j > 1
        puts "wait previous server ..."
        sleep(15)
      end
      puts "*********************** deploy the #{j} server of #{servers.length}: #{server} ***********************"
      deploy(baseDir,war,server,application)
    else
      puts "Server does not exist, you select: #{input}"
    end
    j = j + 1
  end
elsif input == "q"
  puts "Cancel deployment"
elsif input.to_i > 0 && input.to_i <= servers.length
  server = servers[input.to_i - 1]
  if server.chomp.length > 0
    puts "*********************** deploy: #{server} ***********************"
    deploy(baseDir,war,server,application)
  else
    puts "Server does not exist, you select: #{input}"
  end
else
  raise "Error: your choose is not correct: #{input}"
end

puts "**************************** completed! *****************************"

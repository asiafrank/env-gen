#!/usr/bin/ruby
# coding: utf-8
def cmdline(command)
  puts "命令行执行 : #{command}"
  if !system(command)
    raise "命令行执行: #{command} 失败 !"
  end
end
puts "************************* 恢复回滚文件 *******************************"
baseDir = File.expand_path(ARGV[0])
puts "根目录:#{baseDir}"
if !File.directory?(baseDir)
  raise "根目录 : #{baseDir} 不存在"
end
application = ARGV[1]
applicationDir = "#{baseDir}/#{application}"
currentDir = "#{baseDir}/#{application}/current"
releasedDir = "#{baseDir}/#{application}/released"
rollbackedDir = "#{baseDir}/#{application}/rollbacked"
puts "应用名称:#{application}"
puts "应用发布目录:#{applicationDir}"
if !File.exist?(rollbackedDir)
  raise "回滚目录 :#{rollbackedDir} 不存在"
end
#  last rollbacked dir
lastRollbackedDir = ""
Dir["#{rollbackedDir}/*"].each do |f|
  dirName = File.basename(f)
  if lastRollbackedDir == ""
    lastRollbackedDir = dirName
  else 
    if dirName > lastRollbackedDir
      lastRollbackedDir = dirName
    end
  end
end
if lastRollbackedDir == ""
  raise "没有回滚的文件"
end
lastRollbackedDir = "#{rollbackedDir}/#{lastRollbackedDir}"
puts "上次回滚目录 #{lastRollbackedDir}"
puts "************************* 恢复文件 *******************************"
if Dir["#{currentDir}/*"].length > 0
  if !File.exist?(releasedDir)
    raise "已发布目录 :#{releasedDir} 不存在"
  end
  #  record release history
  newReleasedDir = "#{releasedDir}/#{Time.now.to_i}"
  Dir.mkdir(newReleasedDir)
  puts "复制当前发布目录 #{currentDir} 到历史发布目录 #{newReleasedDir}"
  cmdline("cp -r #{currentDir}/* #{newReleasedDir}")
  puts "清空当前目录 #{currentDir}"
  cmdline("rm -r #{currentDir}")
  cmdline("mkdir #{currentDir}")
end
# copy last rollbacked to current
puts "复制上次回滚目录到当前发布目录 #{currentDir}"
cmdline("cp -r #{lastRollbackedDir}/* #{currentDir}")
puts "删除上次回滚目录 #{lastRollbackedDir}"
cmdline("rm -r #{lastRollbackedDir}")
puts "************************* 恢复回滚文件完成 *******************************"

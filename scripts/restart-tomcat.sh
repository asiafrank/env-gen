#!/bin/sh
tomcatHomeDir=`dirname "$1/bin"`
tomcatHome=`cd "$tomcatHomeDir" >/dev/null; pwd`
if [ ! -d "$tomcatHome/webapps" ]
then
  echo "$tomcatHome is not a tomcat home or is not exist"
  exit 1
fi
echo "set env variables"
if [ -f /etc/profile ]; then
        . /etc/profile
fi
if [ -f ~/.bash_profile ]; then
        . ~/.bash_profile
fi

PID=`ps -ef | grep java | grep "$tomcatHome/bin/" | grep -v grep | awk '{print $2}'`
echo "stoping tomcat $tomcatHome, PID = $PID"
$tomcatHome/bin/shutdown.sh
sleep 1
tempPid=`ps -ef | grep java | grep "$tomcatHome/bin/" | grep -v grep | awk '{print $2}'`
waitingToKill=0
while [ ! -z "$tempPid" ]; do
  echo "$tomcatHome still running in pid=$tempPid, wating to stopped in $waitingToKill/10 "
  if [ $waitingToKill -lt 10 ]; then
    sleep 1
    waitingToKill=`expr $waitingToKill + 1`
    tempPid=`ps -ef | grep java | grep "$tomcatHome/bin/" | grep -v grep | awk '{print $2}'`
  else
    break
  fi
done
if [ ! -z "$tempPid" ]; then
  echo "kill $tempPid"
  kill $tempPid
  sleep 1
fi

tempPid=`ps -ef | grep java | grep "/$tomcatHome/bin/" | grep -v grep | awk '{print $2}'`
waitingToForceKill=0
while [ ! -z "$tempPid" ]; do
  echo "$tomcatHome still running in pid=$tempPid, will force killed in $waitingToForceKill/10 "
  if [ $waitingToForceKill -lt 10 ]; then
    sleep 1
    waitingToForceKill=`expr $waitingToForceKill + 1`
    tempPid=`ps -ef | grep java | grep "$tomcatHome/bin/" | grep -v grep | awk '{print $2}'`
  else
    break
  fi
done
if [ ! -z "$tempPid" ]; then
  echo "kill -9 $tempPid"
  kill -9 $tempPid
  sleep 1
fi
tempPid=`ps -ef | grep java | grep "$tomcatHome/bin/" | grep -v grep | awk '{print $2}'`
if [ ! -z "$tempPid" ]; then
  echo "unable to kill $tomcatHome, PID=$tempPid"
  exit 0
fi
echo "tomcat stopped"

echo "starting tomcat"
$tomcatHome/bin/startup.sh
sleep 1
tempPid=`ps -ef | grep java | grep "$tomcatHome/bin/" | grep -v grep | awk '{print $2}'`
watingStartup=0
while [ -z "$tempPid" ]; do
  echo "wating $tomcatHome startup $watingStartup"
  if [ $watingStartup -lt 10 ]; then
    sleep 1
    watingStartup=`expr $watingStartup + 1`
    tempPid=`ps -ef | grep java | grep "$tomcatHome/bin/" | grep -v grep | awk '{print $2}'`
  else
    break
  fi
done
tempPid=`ps -ef | grep java | grep "$tomcatHome/bin/" | grep -v grep | awk '{print $2}'`
if [ -z "$tempPid" ]; then
  echo "wating $tomcatHome startup timeout"
else
  echo "tomcat started at PID=$tempPid"
  echo `grep "shutdown.port" $tomcatHome/conf/catalina.properties`
  echo `grep "http.port" $tomcatHome/conf/catalina.properties`
fi

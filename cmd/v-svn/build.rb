#!/usr/bin/ruby
# coding: utf-8
require "yaml"
def cmdline(command)
  puts "命令行执行 : #{command}"
  if !system(command)
    raise "命令行执行: #{command} 失败 !"
  end
end
baseDir = File.expand_path(ARGV[0])
puts "根目录:#{baseDir}"
if !File.directory?(baseDir)
  raise "根目录 : #{baseDir} 不存在"
end
configFile = "#{baseDir}/publish.yaml"
puts "配置文件：#{configFile}"
if !File.exist?(configFile)
  raise "配置文件 : #{configFile} 不存在"
end
config = YAML.load_file(configFile)
codeRepoUrl = config["sourceSvnUrl"]
sourcesDir = "#{baseDir}/sources"
puts "源代码库路径:#{codeRepoUrl}"
puts "源代码存放目录:#{sourcesDir}"
puts "************************* svn update 代码 *******************************"
puts "更新filter文件"
Dir.chdir("/home/testjava/.filter")
cmdline("svn up")
Dir.chdir(baseDir)
if File.exist?(sourcesDir)
  Dir.chdir(sourcesDir)
  puts "更新 #{sourcesDir} 目录源代码"
  cmdline("svn info")
  cmdline("svn up")
  Dir.chdir(baseDir)
else
  puts "从 #{codeRepoUrl} 检出源代码到 #{sourcesDir}"
  cmdline("svn co #{codeRepoUrl} #{sourcesDir}")
end
puts "************************* 打包部署文件 *******************************"
  Dir.chdir(sourcesDir)
  puts "package binary at #{sourcesDir}"
  cmdline("mvn -Dmaven.test.skip=true -U clean install")
  Dir.chdir(baseDir)
puts "************************* 打包部署文件完成 *******************************"

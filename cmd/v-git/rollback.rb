#!/usr/local/bin/ruby
# coding: utf-8
def cmdline(command)
  puts "command execution : #{command}"
  if !system(command)
    raise "command execution: #{command} fail !"
  end
end
puts "************************* rollback *******************************"
baseDir = File.expand_path(ARGV[0])
puts "base directory:#{baseDir}"
if !File.directory?(baseDir)
  raise "base directory : #{baseDir} does not exist"
end
application = ARGV[1]
applicationDir = "#{baseDir}/#{application}"
currentDir = "#{baseDir}/#{application}/current"
releasedDir = "#{baseDir}/#{application}/released"
rollbackedDir = "#{baseDir}/#{application}/rollbacked"
puts "application name:#{application}"
puts "application directory:#{applicationDir}"
if !File.exist?(releasedDir)
  raise "released dir :#{releasedDir} not exist"
end
#  last released dir
lastReleasedDir = ""
Dir["#{releasedDir}/*"].each do |f|
  dirName = File.basename(f)
  if lastReleasedDir == ""
    lastReleasedDir = dirName
  else 
    if dirName > lastReleasedDir
      lastReleasedDir = dirName
    end
  end
end
if lastReleasedDir == ""
  raise "no file released"
end
lastReleasedDir = "#{releasedDir}/#{lastReleasedDir}"
puts "last released directory #{lastReleasedDir}"
puts "************************* rollback *******************************"
if Dir["#{currentDir}/*"].length > 0
  if !File.exist?(rollbackedDir)
    raise "rollback directory :#{rollbackedDir} does not exist"
  end
  #  record rollback history
  newRollbackedDir = "#{rollbackedDir}/#{Time.now.to_i}"
  Dir.mkdir(newRollbackedDir)
  puts "copy from #{currentDir} to #{newRollbackedDir}"
  cmdline("cp -r #{currentDir}/* #{newRollbackedDir}")
  puts "clean current directory清空当前发布目录 #{currentDir}"
  cmdline("rm -r #{currentDir}")
  cmdline("mkdir #{currentDir}")
end
# copy last released to current
puts "copy last released directory to #{currentDir}"
cmdline("cp -r #{lastReleasedDir}/* #{currentDir}")
puts "delete last released directory #{lastReleasedDir}"
cmdline("rm -r #{lastReleasedDir}")
puts "************************* done *******************************"

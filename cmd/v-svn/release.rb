#!/usr/bin/ruby
# coding: utf-8
def cmdline(command)
  puts "命令行执行 : #{command}"
  if !system(command)
    raise "命令行执行: #{command} 失败 !"
  end
end
baseDir = File.expand_path(ARGV[0])
puts "根目录:#{baseDir}"
if !File.directory?(baseDir)
  raise "根目录 : #{baseDir} 不存在"
end
puts "************************* 发布打包文件到当前发布目录 *******************************"
application = ARGV[1]
applicationDir = "#{baseDir}/#{application}"
sourcesDir = "#{baseDir}/sources"
currentDir = "#{baseDir}/#{application}/current"
releasedDir = "#{baseDir}/#{application}/released"
rollbackedDir = "#{baseDir}/#{application}/rollbacked"
puts "应用名称:#{application}"
puts "应用发布目录:#{applicationDir}"
targetDir = "#{sourcesDir}/#{application}/target"
#  create history dir structure
if !File.exist?(applicationDir)
  Dir.mkdir(applicationDir)
  Dir.mkdir(currentDir)
  Dir.mkdir(releasedDir)
  Dir.mkdir(rollbackedDir)
end
# find war file
war = ""
Dir["#{targetDir}/#{application}-*.war"].each do |f|
  war = f
end
puts "当前打包文件 : #{war}"
puts "************************* 处理发布历史 *******************************"
if File.exist?(war)
  ignoreRelease = false;
  if Dir["#{currentDir}/*"].length > 0
    # 检查war文件是否有变化
    curWar = ""
    Dir["#{currentDir}/#{application}-*.war"].each do |f|
      curWar = f
    end
    if File.exist?(curWar) && File.mtime(war)==File.mtime(curWar) && File.size(war)==File.size(curWar)
      puts "打包文件和当前文件相同，忽略本次打包文件发布"
      ignoreRelease = true;
    end
    if !ignoreRelease
      # record release history
      newReleasedDir = "#{releasedDir}/#{Time.now.to_i}"
      Dir.mkdir(newReleasedDir)
      puts "复制当前发布目录 从 #{currentDir} 到 发布历史目录 #{newReleasedDir}"
      cmdline("cp -r #{currentDir}/* #{newReleasedDir}")
      puts "清空当前发布目录 #{currentDir}"
      cmdline("rm -r #{currentDir}")
      cmdline("mkdir #{currentDir}")
    end
  end
  if !ignoreRelease
  # copy war to current file
  puts "复制打包文件到当前发布目录 #{currentDir}"
  cmdline("cp -p #{war} #{currentDir}/#{File.basename(war)}")
  end
else
  raise "打包文件 #{application}-x.x.x.war 在目录 #{targetDir} 中不存在"
end
puts "************************* 发布打包文件到当前发布目录完成 *******************************"
